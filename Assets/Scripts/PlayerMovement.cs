﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour {

    public float m_Speed = 0.5f;

    private string m_MovementAxisName = "Horizontal";
    private Rigidbody2D m_Rigidbody;
    private float m_MovementInput;
    private float m_HorizontalBound = 10f;
    private PlayerHealth m_Health;
    


    private void Awake()
    {
        //Get and store ref of rigidbody
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Health = GetComponent<PlayerHealth>();
    }

    private void OnEnable()
    {
        //physics apply to rigidbody when enabled
        m_Rigidbody.isKinematic = false;
    }

    private void OnDisable()
    {
        //physics don't apply to rigidbody when disabled
        m_Rigidbody.isKinematic = true;
    }

    private void Update()
    {
        //Get user input "amount" per tick
        m_MovementInput = Input.GetAxis(m_MovementAxisName);
    }

    private void FixedUpdate()
    {
        //move every physics tick
        if(!m_Health.IsDead())
            Move();
    }

    private void Move()
    {
        //move horizontally according to input
        //only if valid new position
        float x = m_Rigidbody.position.x + m_MovementInput * m_Speed;
        if (Mathf.Abs(x) <= m_HorizontalBound)
        {
            m_Rigidbody.MovePosition(new Vector2(x, 0f));
            //Debug.Log("Rigidbody at: " + m_Rigidbody.transform.position);
        }
    }
}
