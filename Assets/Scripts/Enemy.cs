﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    private Rigidbody2D m_Rigidbody;
    private float m_HorizontalBound = 11f;
    private float m_VerticalBound = 5.6f;
    public bool m_GoingRight = false;
    public bool m_GoesDown = false;
    public bool m_IsAlive = true;

    private void Awake()
    {
        //get rigidbody ref
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_IsAlive = true;
    }

    private void OnEnable()
    {
        //physics apply to object
        m_Rigidbody.isKinematic = false;
        
    }

    private void OnDisable()
    {
        //physics doesn't apply to object
        m_Rigidbody.isKinematic = true;
    }

    private void FixedUpdate()
    {
        //check for valid position - if invalid, move down and opposite direction
        if (m_Rigidbody.position.x >= m_HorizontalBound)
        {
            //to the right of screen
            Debug.Log("Enemy to the right of screen!");
            m_GoingRight = true;
            m_GoesDown = true;
        }
        else if (m_Rigidbody.position.x <= -m_HorizontalBound)
        {
            //to the left of screen
            Debug.Log("Enemy to the left of screen!");
            m_GoingRight = false;
            m_GoesDown = true;
        }
        else
            m_GoesDown = false;
    }

    public void Respawn()
    {
        //respawn to a random y position and random x position, and reset its velocity
        //float randY = Random.Range(-4.30f, 4.30f);
        float randX = Random.Range(-9.57f, 9.57f);
        float randY = 4.30f;
        
        Debug.Log("Enemy will be respawned at: " + randX + ", " + randY);
        m_Rigidbody.MovePosition(new Vector2(randX, randY));
        m_Rigidbody.gravityScale = 0f;
        m_IsAlive = true;
    }
    

    public void OnEnemyDeath()
    {
        Debug.Log("I was killed on " + transform.position);
        m_IsAlive = false;
        Destroy(gameObject);
    }

}
