﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour {

    public Rigidbody2D m_Bullet;
    public Transform m_FireTransform;

    private float m_CurrentLaunchForce = 10f;
    private Enemy m_Enemy;

    private void Awake()
    {
        m_Enemy = GetComponent<Enemy>();
        //m_Enemy.m_IsAlive = true;
    }

    private void Update()
    {
        if(m_Enemy.m_IsAlive)
        {
            //Shoot randomly
            int random = Random.Range(0, 950);
            if (random  == 900)
                Shoot();
        }
    }


    private void Shoot()
    {
        Rigidbody2D bulletInstance = Instantiate(m_Bullet, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody2D;
        bulletInstance.velocity = m_CurrentLaunchForce * m_FireTransform.up;
        Bullet bullet = bulletInstance.GetComponent<Bullet>();
        bullet.SetSource("Enemy");
    }


}
