﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

[Serializable]
public class PlayerManager {

    public Color m_PlayerColor;

    public Transform m_SpawnPoint;
    [HideInInspector] public Text m_Text;
    [HideInInspector] public string m_ColoredPlayerText;
    [HideInInspector] public GameObject m_Instance;

    [HideInInspector] public PlayerHealth m_Health;
    [HideInInspector] public PlayerMovement m_Movement;
    [HideInInspector] public PlayerShooting m_Shooting;
    [HideInInspector] public PlayerScore m_Score;

    //add canvas later

    public void Setup()
    {
        m_Movement = m_Instance.GetComponent<PlayerMovement>();
        m_Shooting = m_Instance.GetComponent<PlayerShooting>();
        m_Health = m_Instance.GetComponent<PlayerHealth>();
        m_Score = m_Instance.GetComponent<PlayerScore>();

        m_ColoredPlayerText = "<color=#" + ColorUtility.ToHtmlStringRGB(m_PlayerColor) + ">PLAYER</color>";
        m_Text = m_Instance.GetComponentInChildren<Text>();
    }

    public void EnableControl()
    {
        m_Movement.enabled = true;
        m_Shooting.enabled = true;
        m_Score.enabled = true;
    }

    public void DisableControl()
    {
        m_Movement.enabled = false;
        m_Shooting.enabled = false;
        m_Score.enabled = false;
    }

    public void Reset()
    {
        m_Instance.transform.position = m_SpawnPoint.position;
        m_Instance.transform.rotation = m_SpawnPoint.rotation;

        m_Instance.SetActive(false);
        m_Instance.SetActive(true);
    }

}
