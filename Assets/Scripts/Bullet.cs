﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {


    private float m_VerticalBound = 5.8f;
    private PlayerScore m_Score;
    private string m_Source;

    private void Awake()
    {
        //Collider2D[] colliders = Physics2D.OverlapAre
        Collider2D[] colliders = Physics2D.OverlapAreaAll(new Vector2(-10f, -2.21f), new Vector2(10f, -4.76f));

        for(int i=0; i < colliders.Length; ++i)
        {
            //make sure target has rigidbody
            Rigidbody2D targetRigidbody = colliders[i].GetComponent<Rigidbody2D>();
            if (!targetRigidbody)
                continue;

            PlayerScore targetScore = colliders[i].GetComponent<PlayerScore>();
            if (!targetScore)
                continue;
            else
            {
                //if valid score, store that and immediately exit awake
                m_Score = targetScore;
                return;
            }
        }

        
    }

    private void OnEnable()
    {
        //Debug.Log(GetComponent<Collider2D>().tag);
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        /*
        //determine whether source was a player or an enemy shooting
        if (m_Source == "Enemy")
        {
            Debug.Log("I am an enemy's bullet!");
        }
        else if (m_Source == "Player")
        {
            Debug.Log("I am a player's bullet!");
        }
        */


        //bullet hits an enemy
        if (collision.tag == "Enemy" && m_Source != "Enemy")
        {
            //destroy bullet, call OnEnemyDeath() for that particular enemy
            Debug.Log("Bullet Collision with enemy at " + collision.transform.position);
            Destroy(gameObject, 0f);
            Enemy enemy = collision.gameObject.GetComponent<Enemy>();
            enemy.OnEnemyDeath();
            m_Score.IncreaseScore();

            //Debug.Log();
        }
        //bullet hits player
        else if(collision.tag == "Player" && m_Source != "Player")
        {
            Debug.Log("Enemy has landed a hit on player at " + collision.transform.position);
            PlayerHealth health = collision.gameObject.GetComponent<PlayerHealth>();
            health.DecreaseHealth();
            health.UpdateHealthUI();
            m_Score.DecreaseScore();
        }
        //bullet hits bunker
        else if(collision.tag == "Bunker")
        {
            Debug.Log("Bullet Collision with bunker at " + collision.transform.position);
            Bunker bunker = collision.gameObject.GetComponent<Bunker>();
            bunker.OnDamage();
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        //if bullet goes out of screen, automatically delete it - no point hogging memory with it
        if(Mathf.Abs(transform.position.y) >= m_VerticalBound)
        {
            Debug.Log("Bullet went out of bounds at " + transform.position + " and was destroyed");
            Destroy(gameObject, 0f);
        }
    }

    public void SetSource(string text)
    {
        m_Source = text;
    }
}
