﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public float m_StartDelay = 3f;
    public float m_EndDelay = 3f;
    public GameObject m_EnemyPrefab;
    public GameObject m_PlayerPrefab;
    public GameObject m_BunkerPrefab;
    private Text m_EndMessage;

    public PlayerManager m_Player;
    public EnemyManager[] m_Enemies = { null, null };
    public BunkerManager[] m_Bunkers = { null };
    private WaitForSeconds m_StartWait;
    private WaitForSeconds m_EndWait;
    private bool m_PlayerWon;

    private void Start()
    {
        m_StartWait = new WaitForSeconds(m_StartDelay);
        m_EndWait = new WaitForSeconds(m_EndDelay);

        SpawnAllEnemies();
        SpawnPlayer();
        SpawnBunkers();
        StartCoroutine(GameLoop());
    }

    private void SpawnBunkers()
    {
        for(int i = 0; i < m_Bunkers.Length; ++i)
        {
            m_Bunkers[i].m_Instance = Instantiate(m_BunkerPrefab, m_Bunkers[i].m_SpawnPoint.position, m_Bunkers[i].m_SpawnPoint.rotation) as GameObject;
            m_Bunkers[i].Setup();
            m_Bunkers[i].Enable();
        }
    }

    private void SpawnAllEnemies()
    {
        for(int i = 0; i < m_Enemies.Length; ++i)
        {
            m_Enemies[i].m_Instance = Instantiate(m_EnemyPrefab, m_Enemies[i].m_SpawnPoint.position, m_Enemies[i].m_SpawnPoint.rotation) as GameObject;
            m_Enemies[i].m_EnemyNumber = i + 1;
            m_Enemies[i].Setup();
        }
    }

    private void SpawnPlayer()
    {
        m_Player.m_Instance = Instantiate(m_PlayerPrefab, m_Player.m_SpawnPoint.position, m_Player.m_SpawnPoint.rotation);
        m_Player.Setup();
    }

    private void MoveAllEnemies()
    {
        for(int i = 0; i < m_Enemies.Length; ++i)
        {
            if (!m_Enemies[i].IsAlive())
                continue;

            float amountX = (m_Enemies[i].m_Enemy.m_GoingRight) ? -0.08f : 0.08f;
            float amountY = (m_Enemies[i].m_Enemy.m_GoesDown) ? -0.3f : 0f;
            m_Enemies[i].MoveTo(new Vector2(m_Enemies[i].m_Instance.transform.position.x + amountX, m_Enemies[i].m_Instance.transform.position.y + amountY));
        }
    }

    private IEnumerator GameLoop()
    {
        yield return StartCoroutine(RoundStarting());
        yield return StartCoroutine(RoundPlaying());
        yield return StartCoroutine(RoundEnding());

        SceneManager.LoadScene("Main");

    }

    private void ResetBunkers()
    {
        for (int i = 0; i < m_Bunkers.Length; ++i)
        {
            m_Bunkers[i].Reset();
            m_Bunkers[i].Enable();
        }
    }

    private IEnumerator RoundStarting()
    {
        m_EndMessage = m_Player.m_Text;
        ResetAllEnemiesAndPlayer();
        ResetBunkers();
        DisablePlayerAndEnemyControl();

        //canvas thing here
        m_EndMessage.text = "Round start!";
        yield return m_StartWait;
    }

    private IEnumerator RoundPlaying()
    {
        m_EndMessage.text = "";
        EnablePlayerAndEnemyControl();
        
        //while player isnt dead or one enemy+ is alive
        while(!NoEnemyLeft() && !m_Player.m_Health.IsDead())
        {
            MoveAllEnemies();
            yield return null;
        }

        m_PlayerWon = (m_Player.m_Health.IsDead()) ? false : true;
    }

    private void DisableBunkers()
    {
        for (int i = 0; i < m_Bunkers.Length; ++i)
            m_Bunkers[i].Disable();
    }

    private IEnumerator RoundEnding()
    {
        DisablePlayerAndEnemyControl();
        DisableBunkers();

        //end message bla bla bla
        string message = EndMessage();
        m_EndMessage.text = message;

        yield return m_EndWait;
    }
    
    private bool NoEnemyLeft()
    {
        int numEnemiesLeft = 0;
        for( int i = 0; i < m_Enemies.Length; ++i)
        {
            if (m_Enemies[i].IsAlive())
                ++numEnemiesLeft;
        }

        return numEnemiesLeft <= 0;
    }

    public void DisablePlayerAndEnemyControl()
    {
        for (int i = 0; i < m_Enemies.Length; ++i)
        {
            if (!m_Enemies[i].m_Enemy)
                continue;

            m_Enemies[i].m_Enemy.enabled = false;
            m_Enemies[i].m_Shooting.enabled = false;
        }
        m_Player.m_Shooting.enabled = false;
        m_Player.m_Movement.enabled = false;
    }

    public void EnablePlayerAndEnemyControl()
    {
        for (int i = 0; i < m_Enemies.Length; ++i)
        {
            if (!m_Enemies[i].m_Enemy)
                continue;

            m_Enemies[i].m_Enemy.enabled = true;
            m_Enemies[i].m_Shooting.enabled = true;
        }
        m_Player.m_Shooting.enabled = true;
        m_Player.m_Movement.enabled = true;
    }

    public void ResetAllEnemiesAndPlayer()
    {
        for (int i = 0; i < m_Enemies.Length; ++i)
        {
            m_Enemies[i].Reset();
        }
        m_Player.Reset();
    }

    private string EndMessage()
    {
        string message = "Game Over\n";

        if (m_PlayerWon)
        {
            message += " You win!";
        }
        else
            message += "You lose!";

        return message;
    }
}
