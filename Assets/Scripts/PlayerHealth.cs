﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour {

    public int m_InitialHealth = 3;
    public Image[] m_HealthImage;
    public Sprite m_DeadSprite;
    public Sprite m_AliveSprite;
    public Text m_GameText;

    private int m_CurrentHealth;
    private Rigidbody2D m_Rigidbody;
    private SpriteRenderer m_Renderer;
    private int m_Score = 0;

    private void Awake()
    {
        //Store ref to rigidbody, and sprite renderer
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Renderer = GetComponent<SpriteRenderer>();
    }

    private void OnEnable()
    {
        //set health to starting health
        m_CurrentHealth = m_InitialHealth;
        UpdateHealthUI();
    }

    public void UpdateHealthUI()
    {
        //Update ui based on current player health - disables all lives up to the player's current lives
        if(m_CurrentHealth >= 0)
        {
            for (int i = m_HealthImage.Length - 1; i >= m_CurrentHealth; --i)
            {
                m_HealthImage[i].enabled = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //collided with an enemy - lose life
        if(collision.tag == "Enemy")
        {
            //Debug.Log("Collision with enemy at " + collision.GetComponent<Collider2D>().transform.position);
            --m_CurrentHealth;
            UpdateHealthUI();
            if (m_CurrentHealth <= 0)
            {
                OnPlayerDeath();
            }
        }
    }

    private void OnPlayerDeath()
    {
        //player has died, change model, show death message, wait for some time, then back to main scene
        m_Renderer.sprite = m_DeadSprite;
        //Debug.Log("OnPlayerDeath() called");
    }

    public bool IsDead()
    {
        return m_CurrentHealth <= 0;
    }

    public void DecreaseHealth()
    {
        --m_CurrentHealth;
    }

    private void Update()
    {
        if (IsDead())
            OnPlayerDeath();
    }
}
