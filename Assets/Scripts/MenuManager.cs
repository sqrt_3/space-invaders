﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

	public void LoadGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ShowCredits()
    {
        SceneManager.LoadScene("Credits");
        StartCoroutine(CreditsReturn());
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Credits"))
            StartCoroutine(CreditsReturn());
    }

    private IEnumerator CreditsReturn()
    {
        //yield return new WaitForSeconds(10f);
        
        yield return new WaitForSeconds(10f);
        SceneManager.LoadScene("Main");
        yield return null; 
    }
}
