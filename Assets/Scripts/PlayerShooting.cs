﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour {

    public Rigidbody2D m_Bullet;
    public Transform m_FireTransform;
    public float m_MinLaunchForce = 5f;
    public float m_MaxLaunchForce = 10f;
    public float m_MaxChargeTime = 0.50f;

    private string m_ShootingAxisName = "Shooting";
    private float m_CurrentLaunchForce;
    private float m_ChargeSpeed;
    private bool m_Fired = false;
    private PlayerHealth m_Health;

    private void Awake()
    {
        m_Health = GetComponent<PlayerHealth>();
    }

    private void OnEnable()
    {
        m_CurrentLaunchForce = m_MinLaunchForce;
    }

    private void Start()
    {
        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
    }

    private void Update()
    {
        if(!m_Health.IsDead())
        {
            if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired)
            {
                //at max charge, not fired
                m_CurrentLaunchForce = m_MaxLaunchForce;
                Shoot();
            }
            else if (Input.GetButtonDown(m_ShootingAxisName))
            {
                //pressed fire for first time
                m_Fired = false;
                m_CurrentLaunchForce = m_MinLaunchForce;

                //audio
            }
            else if (Input.GetButton(m_ShootingAxisName) && !m_Fired)
            {
                //holding button, not fired
                m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;

            }
            else if (Input.GetButtonUp(m_ShootingAxisName) && !m_Fired)
            {
                //released button, not fired yet
                Shoot();
            }
        }
    }

    private void Shoot()
    {
        m_Fired = true;
        Rigidbody2D bulletInstance = Instantiate(m_Bullet, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody2D;
        bulletInstance.velocity = m_CurrentLaunchForce * m_FireTransform.up;
        Bullet bullet = bulletInstance.GetComponent<Bullet>();
        bullet.SetSource("Player");

        
    }
}
