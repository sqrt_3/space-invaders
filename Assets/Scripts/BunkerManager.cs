﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class BunkerManager {

    public Transform m_SpawnPoint;
    [HideInInspector] public GameObject m_Instance;

    [HideInInspector]  public Bunker m_Bunker;

    public Material[] m_Materials;


	public void Setup ()
    {
        m_Bunker = m_Instance.GetComponent<Bunker>();
        SetupMaterials();
	}

    public void Disable()
    {
        m_Bunker.enabled = false;
        m_Instance.SetActive(false);
    }

    public void Enable()
    {
        m_Bunker.enabled = true;
        m_Instance.SetActive(true);
    }
	
    public void Reset()
    {
        m_Instance.transform.position = m_SpawnPoint.position;
        m_Instance.transform.rotation = m_SpawnPoint.rotation;

        m_Instance.SetActive(false);
        m_Instance.SetActive(true);
    }
    
    private void SetupMaterials()
    {
        for(int i=0; i < m_Materials.Length; ++i)
        {
            m_Bunker.m_Materials[i] = m_Materials[i];
        }
        m_Bunker.SetupMaterial();
    }
}
