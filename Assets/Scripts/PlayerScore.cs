﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour {

    public int m_StartScore;
    public Text m_ScoreText;
    private int m_CurrentScore;

    private void OnEnable()
    {
        UpdateScoreUI();
    }

    private void UpdateScoreUI()
    {
        m_ScoreText.text = "Score     " + m_CurrentScore;
    }

    public void IncreaseScore()
    {
        ++m_CurrentScore;
        UpdateScoreUI();
    }

    public void DecreaseScore()
    {
        --m_CurrentScore;
        UpdateScoreUI();
    }
}
