﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class EnemyManager {


    [HideInInspector]  public GameObject m_Instance;
    [HideInInspector]  public int m_EnemyNumber = 1;

    public Transform m_SpawnPoint;

    [HideInInspector] public Enemy m_Enemy;
    [HideInInspector] public EnemyShooting m_Shooting;

    public void Setup()
    {
        m_Enemy = m_Instance.GetComponent<Enemy>();
        m_Shooting = m_Instance.GetComponent<EnemyShooting>();
        m_Enemy.m_IsAlive = true;
    }

    public void Reset()
    {
        m_Instance.transform.position = m_SpawnPoint.position;
        m_Instance.transform.rotation = m_SpawnPoint.rotation;

        m_Instance.SetActive(false);
        m_Instance.SetActive(true);
        m_Enemy.m_IsAlive = true;
    }

    public void MoveTo(Vector2 point)
    {
        m_Instance.GetComponent<Rigidbody2D>().MovePosition(point);
    }

    public bool IsAlive()
    {
        return m_Enemy.m_IsAlive;
    }
}
