﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bunker : MonoBehaviour {

    
    public int m_StartingHealth = 5;
    public Material[] m_Materials = { null, null, null, null, null };

    private int m_CurrentHealth;
    private Rigidbody2D m_Rigidbody;
    private SpriteRenderer m_Renderer;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Renderer = GetComponent<SpriteRenderer>();
        m_CurrentHealth = m_StartingHealth;
    }

    public void SetupMaterial()
    {
        m_Renderer.material = m_Materials[m_StartingHealth - 1];
    }


    public void OnDamage()
    {
        --m_CurrentHealth;
        m_Renderer.material = m_Materials[m_CurrentHealth];
        if (m_CurrentHealth <= 0)
            gameObject.SetActive(false);
            //Destroy(gameObject);
    }
}
