# Space Invaders

Space Invaders game - classic remake made with Unity and C#.

##What's new?
* 3 Scenes are in the game: The main menu scene, the game scene, and the credits scene.
* UI System includes: heart sprites representing player's health, and a gametext with the player's score
* Players would start of on the main menu screen which would let them play the game, check out the credits, or quit the game.
* Bunkers have been implemented: they can take up to 5 enemy/player hits and will change color based on their current health
* Both enemies/player can shoot bullets: enemies will randomly shoot at specific times to make gameplay more challenging
* Music has been added into the Main & Credits scenes, as well as the Game scene.

####Credits
* Unity - software
* Megaman2 - Castlevania theme
* Killer Instinct - Main theme
* Matias Lago - the game itself






